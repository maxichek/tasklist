var tasks = [
    {
        id: 0,
        deadline: 1555687800,
        title: 'Помыть посуду',
        is_complete: false
    },
    {
        id: 1,
        deadline: 1555932000,
        title: 'Сходить в магазин',
        is_complete: true
    }
];
const container = document.getElementsByClassName('container')[0];

renderAll();

function renderTask(task) {
    container.innerHTML += `
        <div class="task">
            <div class="task-info">
                <input type="checkbox" ${task.is_complete ? 'checked' : ''}>
                <span class="task-deadline">${convertToDate(task.deadline)}</span>
            </div>
            <span class="task-name">${task.title}</span>

            <div class="task-delete">
                <i class="fas fa-times"></i>
            </div>
        </div>
    `;
}

function renderAll() {
    container.innerHTML = '';
    tasks.forEach(item => renderTask(item));

    // for(var i = 0; i < tasks.length; i++) {
    //    renderTask(tasks[i]);
    // }
}

function convertToDate(time) {
    moment.locale('ru');
    return moment.unix(time).format('lll');
}

